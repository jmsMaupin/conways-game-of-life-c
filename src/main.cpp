#include<iostream>
#include <GL/gl.h>
#include <GL/glut.h>
#include "../headers/Cell.h"

using namespace std;

const float LEFT_CLIP     = -100.0,
            RIGHT_CLIP    =  100.0,
            BOTTOM_CLIP   = -100.0,
            TOP_CLIP      =  100.0;

const int   SCREEN_WIDTH  = 600,
            SCREEN_HEIGHT = 600,
            ROW_COUNT     = 8;

const float CELL_PADDING  = 0.5f,
            GRID_SIZE     = 160.0f;

int   numberOfCells = ROW_COUNT * ROW_COUNT;

float length    = (GRID_SIZE) / float(ROW_COUNT);
float leftMostX = (LEFT_CLIP + length/2.0f) + ((RIGHT_CLIP - LEFT_CLIP) - GRID_SIZE)/2.0f;
float topMostY  = (TOP_CLIP - length/2.0f) - ((TOP_CLIP - BOTTOM_CLIP) - GRID_SIZE)/2.0f;

void render(){
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    {
        //drawing code goes here
        Square(0,0,160,0,.2f,1.0f).draw();
    }
    glFlush();
}

void resize(int width, int height) {
    glutReshapeWindow(SCREEN_WIDTH, SCREEN_HEIGHT);
}

int main(int argc, char** argv) {
    //program arguments
    //-n N sets the width and height of the board (N * N)
    //--board "boardstate" sets the board state. expected input is a string of 1 and 0's 1 represents alive, 0 is dead.
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(SCREEN_WIDTH,SCREEN_HEIGHT);
    glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH)-SCREEN_WIDTH)/2,
                           (glutGet(GLUT_SCREEN_HEIGHT)-SCREEN_HEIGHT)/2);
    glutCreateWindow("OpenGL - First window demo");

    glClearColor(0.0, 0.0, 0.0, 0.0); // black background
    glMatrixMode(GL_PROJECTION); // setup viewing projection
    glLoadIdentity(); // start with identity matrix
    glOrtho(LEFT_CLIP, RIGHT_CLIP, BOTTOM_CLIP, TOP_CLIP, -1.0, 1.0); //
        glutDisplayFunc(render);
        glutReshapeFunc(resize);
    glutMainLoop();
    return 0;
}