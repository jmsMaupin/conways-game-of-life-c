//
// Created by jmsgears on 5/3/16.
//

#include "../headers/Square.h"

Square::Square(float x, float y, float length) : Square(x, y, length, 1, 1, 1){
}

Square::Square(float x, float y, float length, float r, float g, float b) : Drawable(new Vertex[4], 4, r, g, b){
    vertices[0] = Vertex(x - length / 2, y - length / 2);
    vertices[1] = Vertex(x + length / 2, y - length / 2);
    vertices[2] = Vertex(x + length / 2, y + length / 2);
    vertices[3] = Vertex(x - length / 2, y + length / 2);
}