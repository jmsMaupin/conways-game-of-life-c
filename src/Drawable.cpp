//
// Created by jmsgears on 5/1/16.
//

#include <GL/gl.h>
#include <iostream>
#include "../headers/Drawable.h"

Drawable::Drawable(Vertex *vertices, int numOfVertices) : Drawable(vertices, numOfVertices, 1, 1, 1) { }
Drawable::Drawable(Vertex *vertices, int numOfVertices, float r, float g, float b) {
    this->vertices = vertices;
    this->numOfVertices = numOfVertices;
    this->r = r;
    this->g = g;
    this->b = b;
}

void Drawable::update() { }

void Drawable::draw() const {
    glBegin(GL_POLYGON);
    glColor3f(r,g,b);
    for(int i = 0; i < numOfVertices; ++i)
        glVertex2f(vertices[i].x, vertices[i].y);
    glEnd();
}