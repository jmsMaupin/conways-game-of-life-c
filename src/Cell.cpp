//
// Created by jmsgears on 5/8/16.
//

#include "../headers/Cell.h"

Cell::Cell();
Cell::Cell(float x, float y, float length) : Square(x, y, length, 1, 1, 1){
    setLife(false);
    this->x = x;
    this->y = y;
}

float Cell::getX() { return this->x; }
float Cell::getY() { return this->y; }

void Cell::setLife(bool isAlive) {
    if(isAlive){
        r = 1;
        g = 1;
        b = 1;
    }else{
        r = 0;
        g = 0;
        b = 0;
    }
}