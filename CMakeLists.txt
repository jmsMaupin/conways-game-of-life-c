cmake_minimum_required(VERSION 3.5)
project(ConwaysGameOfLife)

FIND_PACKAGE(GLUT)
FIND_PACKAGE(OpenGL)
SET(GL_LIBS ${GLUT_LIBRARY} ${OPENGL_LIBRARY})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")


set(SOURCE_FILES src/main.cpp src/Drawable.cpp headers/Drawable.h src/Vertex.cpp headers/Vertex.h src/Square.cpp headers/Square.h src/Cell.cpp headers/Cell.h src/Frame.cpp headers/Frame.h)
add_executable(ConwaysGameOfLife ${SOURCE_FILES})
target_link_libraries(ConwaysGameOfLife ${GL_LIBS} m)