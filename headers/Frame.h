//
// Created by jmsgears on 5/14/16.
//

#ifndef CONWAYSGAMEOFLIFE_FRAME_H
#define CONWAYSGAMEOFLIFE_FRAME_H


#include "Cell.h"

class Frame {
private:
    Cell *cells[];
    int size;
public:
    Frame();
    Frame(int length);
    Frame(int length, float grid_size);
    Frame(int number_of_rows, float grid_length, float top_of_grid, float left_of_grid);
    Frame(Frame previous);

    void draw();
};


#endif //CONWAYSGAMEOFLIFE_FRAME_H
