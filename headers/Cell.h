//
// Created by jmsgears on 5/8/16.
//

#ifndef CONWAYSGAMEOFLIFE_CELL_H
#define CONWAYSGAMEOFLIFE_CELL_H

#include "Square.h"

class Cell : public Square{
private:
    bool isAlive;
    float x, y;
public:
    Cell();
    Cell(float x, float y, float length);
    void setLife(bool isAlive);

    float getX();
    float getY();
};


#endif //CONWAYSGAMEOFLIFE_CELL_H
