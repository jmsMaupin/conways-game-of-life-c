//
// Created by jmsgears on 5/1/16.
//

#ifndef CONWAYSGAMEOFLIFE_DRAWNOBJECT_H
#define CONWAYSGAMEOFLIFE_DRAWNOBJECT_H

#include "Vertex.h"
class Drawable {
protected:
    float r, g, b;
    int numOfVertices;
    Vertex *vertices;

public:
    Drawable(Vertex *vertices, int numOfVertices);
    Drawable(Vertex *vertices, int numOfVertices, float r, float g, float b);

    virtual void update();
    virtual void draw() const;

};


#endif //CONWAYSGAMEOFLIFE_DRAWNOBJECT_H
