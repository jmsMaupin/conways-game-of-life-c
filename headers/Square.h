//
// Created by jmsgears on 5/3/16.
//

#ifndef CONWAYSGAMEOFLIFE_SQUARE_H
#define CONWAYSGAMEOFLIFE_SQUARE_H


#include "Drawable.h"

class Square : public Drawable{

public:
    Square(float x, float y, float length);
    Square(float x, float y, float length, float r, float g, float b);
};


#endif //CONWAYSGAMEOFLIFE_SQUARE_H
