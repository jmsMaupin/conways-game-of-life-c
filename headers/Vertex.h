//
// Created by jmsgears on 5/1/16.
//

#ifndef CONWAYSGAMEOFLIFE_VERTEX_H
#define CONWAYSGAMEOFLIFE_VERTEX_H


struct Vertex {
    Vertex();
    Vertex(float x, float y);
    float x, y;
};


#endif //CONWAYSGAMEOFLIFE_VERTEX_H
