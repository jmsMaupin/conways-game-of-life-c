life.exe: main.o Drawable.o Square.o Vertex.o Cell.o
	g++ -std=c++11 -o life.exe main.o Drawable.o Square.o Vertex.o Cell.o -lGL -lGLU -lglut

main.o: ./src/main.cpp
	g++ -std=c++11 -c ./src/main.cpp

Drawable.o: ./src/Drawable.cpp ./headers/Drawable.h
	g++ -std=c++11 -c ./src/Drawable.cpp

Square.o: ./src/Square.cpp ./headers/Square.h
	g++ -std=c++11 -c ./src/Square.cpp

Vertex.o: ./src/Vertex.cpp ./headers/Vertex.h
	g++ -std=c++11 -c ./src/Vertex.cpp

Cell.o: ./src/Cell.cpp ./headers/Cell.h
	g++ -std=c++11 -c ./src/Cell.cpp

clean:
	rm main.o Drawable.o Square.o Vertex.o Cell.o life.exe
